import { Component } from '@angular/core';
import { User } from './shared/users.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  users: User[] = [];
  openForm = false;
  openUsers = true;

  onOpenForm() {
    this.openForm = true;
    this.openUsers = false;
  }

  onOpenUsers() {
    this.openUsers = true;
    this.openForm = false;
  }
}

