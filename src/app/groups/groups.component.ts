import { Component, OnInit } from '@angular/core';
import { Group } from '../shared/group.model';
import { GroupService } from '../shared/group.service';
import { UserService } from '../shared/userService';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  arrayGroup: Group[] = [];

  constructor(private groupService: GroupService, private userService: UserService) { }

  ngOnInit(): void {
    this.arrayGroup = this.groupService.getGroup();
    this.groupService.groupItemsChange.subscribe((arrayGroup: Group[]) => {
      this.arrayGroup = arrayGroup;
    })
  }
}
