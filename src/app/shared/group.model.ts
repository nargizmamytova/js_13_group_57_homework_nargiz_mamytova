import { User } from './users.module';

export class Group{
  private users: User[] = [];
  constructor(
    public groupName: string
    ) {}
  addUser(user: User){
    if(!this.users.includes(user)){
      this.users.push(user);
    }
  }
  getUsers(){
    return this.users.slice();
  }
}

