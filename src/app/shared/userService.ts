import { User } from './users.module';
import { EventEmitter } from '@angular/core';

export class UserService{
  usersChange = new EventEmitter<User>();
  private users: User[] = [
    new User('Ivan','a@mail.ru', true,'admin'),
    new User('Bob','b@mail.ru', false,'user'),
    new User('John','v@mail.ru', true,'admin'),

      ];

  getUsers(){
    return this.users.slice();
  }
  addUser(user: User){
    this.users.push(user);
    this.usersChange.emit();
  }
}
